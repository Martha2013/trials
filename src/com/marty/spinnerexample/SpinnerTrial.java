package com.marty.spinnerexample;




import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class SpinnerTrial extends Activity implements OnItemSelectedListener{
	 Button btnGO;
	 Spinner Spinner1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_trial);
        Spinner1=(Spinner)findViewById(R.id.spinner1);
        Spinner1.setOnItemSelectedListener(this); //this is the interface instance
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.course_array, android.R.layout.simple_spinner_item); //default spinner appearance
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //standard layout defined by platform
        Spinner1.setAdapter(adapter);  //Apply adapter to the spinner
    }


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.spinner_trial, menu);
        return true;
    }


	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
    
}
